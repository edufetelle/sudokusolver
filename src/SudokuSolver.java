/*
 * Sudoku grid 9 x 9 solving program using a recursive algorithm
 * 		built using the acm graphics package from the ACM Java Task Force
 * 		https://cs.stanford.edu/people/eroberts/jtf/
 * 
 */


import acm.program.*;
import acm.graphics.*;
import java.awt.event.*;
import javax.swing.*;


public class SudokuSolver extends GraphicsProgram {
	
	public static final int BOX_SIZE = 150;
	public static final int GAP_SIZE = 2;
	
	public static final int APPLICATION_WIDTH = 600;
	public static final int APPLICATION_HEIGHT = 600;
	
	public void init() {
		
		/* adding controls to the window */
		
		add(new JButton("previous"), SOUTH);
		add(new JButton("next"), SOUTH);
		add(new JButton("1"), EAST);
		add(new JButton("2"), EAST);
		add(new JButton("3"), EAST);
		add(new JButton("4"), EAST);
		add(new JButton("5"), EAST);
		add(new JButton("6"), EAST);
		add(new JButton("7"), EAST);
		add(new JButton("8"), EAST);
		add(new JButton("9"), EAST);
		add(new JButton("clear"), EAST);
		add(new JButton("clear all"), EAST);
		add(new JButton("solve"), SOUTH);
		addActionListeners();
		addKeyListeners();
		addMouseListeners();
		
		/* Drawing the sudoku grid */
		
		add(new GLine(getWidth()/2 - BOX_SIZE / 2 - GAP_SIZE, getHeight()/2 - BOX_SIZE * 3 / 2 - 2 * GAP_SIZE,
				getWidth()/2 - BOX_SIZE / 2 - GAP_SIZE, getHeight()/2 + BOX_SIZE * 3 / 2 + 2 * GAP_SIZE));
		add(new GLine(getWidth()/2 + BOX_SIZE / 2 + GAP_SIZE, getHeight()/2 - BOX_SIZE * 3 / 2 - 2 * GAP_SIZE,
				getWidth()/2 + BOX_SIZE / 2 + GAP_SIZE, getHeight()/2 + BOX_SIZE * 3 / 2 + 2 * GAP_SIZE));
		add(new GLine(getWidth()/2 - BOX_SIZE * 3 / 2 - 2 * GAP_SIZE, getHeight()/2 - BOX_SIZE / 2 - GAP_SIZE,
				getWidth()/2 + BOX_SIZE * 3 / 2 + 2 * GAP_SIZE, getHeight()/2 - BOX_SIZE / 2 - GAP_SIZE));
		add(new GLine(getWidth()/2 - BOX_SIZE * 3 / 2 - 2 * GAP_SIZE, getHeight()/2 + BOX_SIZE / 2 + GAP_SIZE,
				getWidth()/2 + BOX_SIZE * 3 / 2 + 2 * GAP_SIZE, getHeight()/2 + BOX_SIZE / 2 + GAP_SIZE));
		
		int count = 0;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				tileBox[count] = new SudokuTileBox(BOX_SIZE);
				add(tileBox[count], 
						getWidth()/2 - BOX_SIZE * 3 / 2 - 2 * GAP_SIZE + j * BOX_SIZE + 2 * j * GAP_SIZE,
						getHeight()/2 - BOX_SIZE * 3 / 2 - 2 * GAP_SIZE + i * BOX_SIZE + 2 * i * GAP_SIZE);	
				count++;
			}
		}	
		tileBox[0].selectTile(0, 0);
		tileBoxSelected = 0;
		rowOfTileBoxSelected = 0;
		columnOfTileBoxSelected = 0;
		
	}
	
	/* Action Buttons */
	
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand() == "next") {
			selectNext();
		}
		
		if (e.getActionCommand() == "previous") {
			selectPrevious();
		}
		
		if (e.getActionCommand() == "1") {
			tileBox[tileBoxSelected].setNumber(rowOfTileBoxSelected, columnOfTileBoxSelected, 1);
		} else if(e.getActionCommand() == "2") {
			tileBox[tileBoxSelected].setNumber(rowOfTileBoxSelected, columnOfTileBoxSelected, 2);
		} else if(e.getActionCommand() == "3") {
			tileBox[tileBoxSelected].setNumber(rowOfTileBoxSelected, columnOfTileBoxSelected, 3);
		} else if(e.getActionCommand() == "4") {
			tileBox[tileBoxSelected].setNumber(rowOfTileBoxSelected, columnOfTileBoxSelected, 4);
		} else if(e.getActionCommand() == "5") {
			tileBox[tileBoxSelected].setNumber(rowOfTileBoxSelected, columnOfTileBoxSelected, 5);
		} else if(e.getActionCommand() == "6") {
			tileBox[tileBoxSelected].setNumber(rowOfTileBoxSelected, columnOfTileBoxSelected, 6);
		} else if(e.getActionCommand() == "7") {
			tileBox[tileBoxSelected].setNumber(rowOfTileBoxSelected, columnOfTileBoxSelected, 7);
		} else if(e.getActionCommand() == "8") {
			tileBox[tileBoxSelected].setNumber(rowOfTileBoxSelected, columnOfTileBoxSelected, 8);
		} else if(e.getActionCommand() == "9") {
			tileBox[tileBoxSelected].setNumber(rowOfTileBoxSelected, columnOfTileBoxSelected, 9);
		} else if(e.getActionCommand() == "clear") {
			tileBox[tileBoxSelected].setNumber(rowOfTileBoxSelected, columnOfTileBoxSelected, 0);
		}
		
		if (e.getActionCommand() == "solve") {
			tileBox[tileBoxSelected].unselectTile(rowOfTileBoxSelected, columnOfTileBoxSelected);
			isSolvable();
			//tileBox[tileBoxSelected].unselectTile(rowOfTileBoxSelected, columnOfTileBoxSelected);
		}
		
		if (e.getActionCommand() == "clear all") {
			for (int tile = 0; tile < 9; tile++) {
				for (int row = 0; row < 3; row++) {
					for (int column = 0; column < 3; column++) {
						tileBox[tile].setNumber(row, column, 0);	
					}
				}
			}
		}
		
	}
	
	
	/* Key events */
	
	public void keyTyped(KeyEvent e) {
		if (e.getKeyChar() >= '1' && e.getKeyChar() <= '9') {
			tileBox[tileBoxSelected].setNumber(rowOfTileBoxSelected, columnOfTileBoxSelected, e.getKeyChar() - 48);
		}
		if (e.getKeyChar() == 'p') {
			selectPrevious();
		}
		if (e.getKeyChar() == 'n') {
			selectNext();
		}
		if (e.getKeyChar() == '\b') {
			tileBox[tileBoxSelected].setNumber(rowOfTileBoxSelected, columnOfTileBoxSelected, 0);
		}
	}
	
	
	/* Mouse events : tile selection */
	
	public void mouseClicked(MouseEvent e) {
		GObject box = getElementAt(e.getX(), e.getY());
		if (box != null){
			tileBox[tileBoxSelected].unselectTile(rowOfTileBoxSelected, columnOfTileBoxSelected);
			for (int i = 0; i < 9; i++) {
				if (tileBox[i].getX() == box.getX() && tileBox[i].getY() == box.getY()) {
					tileBoxSelected = i;
					break;
				}
			}
			int col = (int) (e.getX() - tileBox[tileBoxSelected].getX()) * 3 / BOX_SIZE;
			int row = (int) (e.getY() - tileBox[tileBoxSelected].getY()) * 3 / BOX_SIZE;
			tileBox[tileBoxSelected].selectTile(row, col);
			rowOfTileBoxSelected = row;
			columnOfTileBoxSelected = col;
		}
	}
	
	
	/* private functions */
	
	private void selectNext() {
		if (columnOfTileBoxSelected < 2) {
			tileBox[tileBoxSelected].unselectTile(rowOfTileBoxSelected, columnOfTileBoxSelected);
			columnOfTileBoxSelected++;
			tileBox[tileBoxSelected].selectTile(rowOfTileBoxSelected, columnOfTileBoxSelected);
		} else if (rowOfTileBoxSelected < 2) {
			tileBox[tileBoxSelected].unselectTile(rowOfTileBoxSelected, columnOfTileBoxSelected);
			rowOfTileBoxSelected++;
			columnOfTileBoxSelected = 0;
			tileBox[tileBoxSelected].selectTile(rowOfTileBoxSelected, columnOfTileBoxSelected);
		} else if (tileBoxSelected < 8) {
			tileBox[tileBoxSelected].unselectTile(rowOfTileBoxSelected, columnOfTileBoxSelected);
			tileBoxSelected++;
			rowOfTileBoxSelected = 0;
			columnOfTileBoxSelected = 0;
			tileBox[tileBoxSelected].selectTile(rowOfTileBoxSelected, columnOfTileBoxSelected);
		}
	}
	
	private void selectPrevious() {
		if (columnOfTileBoxSelected > 0) {
			tileBox[tileBoxSelected].unselectTile(rowOfTileBoxSelected, columnOfTileBoxSelected);
			columnOfTileBoxSelected--;
			tileBox[tileBoxSelected].selectTile(rowOfTileBoxSelected, columnOfTileBoxSelected);
		} else if (rowOfTileBoxSelected > 0) {
			tileBox[tileBoxSelected].unselectTile(rowOfTileBoxSelected, columnOfTileBoxSelected);
			rowOfTileBoxSelected--;
			columnOfTileBoxSelected = 2;
			tileBox[tileBoxSelected].selectTile(rowOfTileBoxSelected, columnOfTileBoxSelected);
		} else if (tileBoxSelected > 0) {
			tileBox[tileBoxSelected].unselectTile(rowOfTileBoxSelected, columnOfTileBoxSelected);
			tileBoxSelected--;
			rowOfTileBoxSelected = 2;
			columnOfTileBoxSelected = 2;
			tileBox[tileBoxSelected].selectTile(rowOfTileBoxSelected, columnOfTileBoxSelected);
		}
	}
	
	private boolean isSolvable() {
		find1stUnassigned();
		if (tileBox[tileBoxSelected].getNumber(rowOfTileBoxSelected, columnOfTileBoxSelected) != 0) return true;
		
		int currentTile = tileBoxSelected;
		int currentRow = rowOfTileBoxSelected;
		int currentColumn = columnOfTileBoxSelected;
		int tileHLocation = currentTile % 3;
		int tileVLocation = currentTile / 3;
		
		for (int i = 1; i <= 9; i++) {
			boolean isAlreadyInTileBox = false;
			boolean isAlreadyInRow = false;
			boolean isAlreadyInColumn = false;
			
			for (int row = 0; row < 3; row++) {
				for (int column = 0; column < 3; column++) {
					if (tileBox[currentTile].getNumber(row, column) == i) isAlreadyInTileBox = true;
				}
			}
			if (!isAlreadyInTileBox) {				
				for (int k = 0; k < 3; k++) {
					for (int column = 0; column < 3; column++) {
						if (tileBox[currentTile - tileHLocation + k].getNumber(currentRow, column) == i) isAlreadyInRow = true;
					}
				}
				if (!isAlreadyInRow) {					
					for (int k = 0; k < 3; k++) {
						for (int row = 0; row < 3; row++) {
							if (tileBox[currentTile - tileVLocation * 3 + k * 3].getNumber(row, currentColumn) == i) isAlreadyInColumn = true;
						}
					}
				}
			}
			
			if (!isAlreadyInTileBox && !isAlreadyInRow && !isAlreadyInColumn) {
				tileBox[currentTile].setNumber(currentRow, currentColumn, i);
				if (isSolvable()) return true;
				else tileBox[currentTile].setNumber(currentRow, currentColumn, 0);
			}
		}
		
		return false;
	}
	
	
	private void find1stUnassigned() {
		tileBox[tileBoxSelected].unselectTile(rowOfTileBoxSelected, columnOfTileBoxSelected);
		tileBoxSelected = 0;
		rowOfTileBoxSelected = 0;
		columnOfTileBoxSelected = 0;
		tileBox[tileBoxSelected].selectTile(rowOfTileBoxSelected, columnOfTileBoxSelected);
		while (tileBox[tileBoxSelected].getNumber(rowOfTileBoxSelected, columnOfTileBoxSelected) != 0
				&& !endIsReached()) {
			selectNext();
		}
	}
	
	private boolean endIsReached() {
		return (tileBoxSelected == 8 && rowOfTileBoxSelected == 2 && columnOfTileBoxSelected == 2);
	}
	
	/* instance variables */
	
	private SudokuTileBox[] tileBox = new SudokuTileBox[9];
	private int tileBoxSelected;
	private int rowOfTileBoxSelected;
	private int columnOfTileBoxSelected;
	
	
}
