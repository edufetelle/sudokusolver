/*
 * Basic sudoku tile box with 3 x 3 squares
 * 		includes public methods to select/deselect a square and set/get its value
 * 
 * Built using the acm graphics package from the ACM Java Task Force
 * 		https://cs.stanford.edu/people/eroberts/jtf/
 * 
 */


import java.awt.Color;
import acm.graphics.*;


public class SudokuTileBox extends GCompound {
	
	/* Initializer */
	
	public SudokuTileBox(double size) {
		double TILE_SIZE = size / 3;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				GRect rect = new GRect(TILE_SIZE, TILE_SIZE);
				rectGrid[i][j] = rect;
				rect.setFilled(false);
				rect.setFillColor(Color.RED);
				add(rect, j*TILE_SIZE, i*TILE_SIZE);
				labelGrid[i][j] = new GLabel("0");
				labelGrid[i][j].setVisible(false);
				labelGrid[i][j].setFont("Times-*-" + (int) TILE_SIZE/2);
				add(labelGrid[i][j], j*TILE_SIZE + TILE_SIZE /2 - labelGrid[i][j].getWidth()/2, 
						i*TILE_SIZE + TILE_SIZE /2 + labelGrid[i][j].getAscent()/2);
			}
		}
	}
	
	
	/* public methods */
	
	public void selectTile(int row, int column) {
		rectGrid[row][column].setFilled(true);
	}
	
	public void unselectTile(int row, int column) {
		rectGrid[row][column].setFilled(false);
	}
	
	public void setNumber(int row, int column, int number) {
		intGrid[row][column] = number;
		labelGrid[row][column].setLabel("" + number);
		labelGrid[row][column].setVisible(number > 0);
	}
	
	public int getNumber(int row, int column) {
		return intGrid[row][column];
	}
	
	
	/* instance variables */
	private GRect[][] rectGrid = new GRect[3][3];
	private GLabel[][] labelGrid = new GLabel[3][3];
	private int[][] intGrid = new int[3][3];
	
}
